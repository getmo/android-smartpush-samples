package br.com.getmo.smartpush;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import br.com.smartpush.Smartpush;
import br.com.smartpush.SmartpushDeviceInfo;
import br.com.smartpush.SmartpushService;

public class MainActivity extends AppCompatActivity {

    static final int MY_PERMISSIONS_REQUEST = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView( R.layout.main );

        Smartpush.subscribe( this );
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager
                .getInstance( this )
                .registerReceiver( mRegistrationBroadcastReceiver,
                        new IntentFilter(
                                SmartpushService.ACTION_SMARTP_REGISTRATION_RESULT ) );
    }

    @Override
    protected void onStop() {
        super.onStop();
        LocalBroadcastManager
                .getInstance( this )
                .unregisterReceiver( mRegistrationBroadcastReceiver );
    }

    private BroadcastReceiver mRegistrationBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent data ) {
            if ( data.getAction().equals( SmartpushService.ACTION_SMARTP_REGISTRATION_RESULT ) ) {
                SmartpushDeviceInfo device =
                        data.getParcelableExtra( SmartpushDeviceInfo.EXTRA_DEVICE_INFO );

                if ( device != null ) {
                    TextView alias = ( TextView ) findViewById( R.id.alias );
                    String message = device.alias;
                    alias.setText( message );

                    ((Button)findViewById( R.id.btnStart )).setEnabled( true );
                } else {
                    TextView alias = ( TextView ) findViewById( R.id.alias );
                    String message = "Fail :(";
                    alias.setText( message );

                    ((Button)findViewById( R.id.btnStart )).setEnabled( false );
                }

                // set your custom TAG here!
            }
        }
    };


    public void onClick( View v ) {
        if ( ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED ) {

            // Request the permissions.
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    MY_PERMISSIONS_REQUEST);

            return;
        }

        startService( new Intent(this, GeoTrackingService.class ) );
    }

    @Override
    public void onRequestPermissionsResult( int requestCode, String permissions[], int[] grantResults ) {
        switch (requestCode ) {
            case MY_PERMISSIONS_REQUEST: {
                // If request is cancelled, the result arrays are empty.
                if ( grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED ) {

                    startService( new Intent(this, GeoTrackingService.class ) );
                } else {
                    Toast.makeText( this, "Ops! Sem essas permissões você não será avisado de ofertas incriveis!", Toast.LENGTH_SHORT ).show();
                }
                return;
            }
        }
    }
}
