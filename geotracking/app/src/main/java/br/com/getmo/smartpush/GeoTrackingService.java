package br.com.getmo.smartpush;

import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import br.com.smartpush.Smartpush;

// https://developers.google.com/cloud-messaging/network-manager
// http://stackoverflow.com/questions/28535703/best-way-to-get-user-gps-location-in-background-in-android

public class GeoTrackingService extends Service implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private GoogleApiClient mGoogleApiClient;
    private boolean mRequestingLocationUpdates;
    private LocationRequest mLocationRequest;
    private Location mCurrentLocation;

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        new Thread( new Runnable() {
            @Override
            public void run() {
                // Create an instance of GoogleAPIClient.
                if ( mGoogleApiClient == null ) {
                    mGoogleApiClient = new GoogleApiClient.Builder( GeoTrackingService.this)
                            .addConnectionCallbacks( GeoTrackingService.this )
                            .addOnConnectionFailedListener( GeoTrackingService.this )
                            .addApi( LocationServices.API )
                            .build();

                    mGoogleApiClient.connect();
                } else {
                    if (!mGoogleApiClient.isConnected()) {
                        mGoogleApiClient.connect();
                    }
                }
            }
        }).start();

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        LocationServices.FusedLocationApi.removeLocationUpdates( mGoogleApiClient, this );
        mGoogleApiClient.disconnect();
        super.onDestroy();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (!mRequestingLocationUpdates) {

            mLocationRequest = new LocationRequest();
            mLocationRequest.setInterval(10000);
            mLocationRequest.setFastestInterval(5000);
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

            if ( ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED ) {
                return;
            }

            LocationServices
                    .FusedLocationApi
                    .requestLocationUpdates( mGoogleApiClient, mLocationRequest, this );

            mRequestingLocationUpdates = true;
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed( @NonNull ConnectionResult connectionResult ) {

    }

    @Override
    public void onLocationChanged( Location location ) {
        mCurrentLocation = location;
        Log.d( "DEBUG", String.valueOf( mCurrentLocation.getLatitude() ) + "," + String.valueOf( mCurrentLocation.getLongitude() ) );
        Smartpush.nearestZone( this, mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude() );
    }
}
