package br.com.getmo.smartpush;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import br.com.smartpush.Smartpush;
import br.com.smartpush.SmartpushDeviceInfo;
import br.com.smartpush.SmartpushService;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.main );

        Smartpush.subscribe( this );
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager
                .getInstance( this )
                .registerReceiver( mRegistrationBroadcastReceiver,
                        new IntentFilter(
                                SmartpushService.ACTION_SMARTP_REGISTRATION_RESULT ) );
    }

    @Override
    protected void onStop() {
        super.onStop();
        LocalBroadcastManager
                .getInstance( this )
                .unregisterReceiver( mRegistrationBroadcastReceiver );
    }

    private BroadcastReceiver mRegistrationBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive( Context context, Intent data ) {

            if ( data.getAction().equals( SmartpushService.ACTION_SMARTP_REGISTRATION_RESULT ) ) {
                SmartpushDeviceInfo device =
                        data.getParcelableExtra( SmartpushDeviceInfo.EXTRA_DEVICE_INFO );

                TextView alias = ( TextView ) findViewById( R.id.alias );
                String message = ( device != null ) ? device.alias : "Fail :(";
                alias.setText( message );

                // set your custom TAG here!

            }
        }
    };
}
